import Vue from 'vue'
import Router from 'vue-router'
// VIEW
import Home from '../views/Home.vue'
import GetStart from '../views/Landing.vue'
import Index from '../views/Index.vue'
import Cart from '../views/Cart.vue'
import DetailProduk from '../views/DetailProduk.vue'
import DetailProduct from '../views/DetailProduct.vue'
import Produk from '../views/Product.vue'
// LAYOUT
import LayoutTombol from './../layouts/Layout'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/getStart',
      name: 'getStart',
      component: GetStart
    },
    {
      path: '/index',
      name: 'Index',
      component: LayoutTombol,
      children: [{
        path: '/beranda',
        name: 'beranda',
        component: Index
      },
      {
        path: '/cart',
        name: 'cart',
        component: Cart
      }]
    },
    {
      path: '/detailProduk/:id',
      name: 'detailProduk',
      component: DetailProduk
    },
    {
      path: '/detailProduct/:id',
      name: 'detailProduct',
      component: DetailProduct
    },
    {
      path: '/product/:id',
      name: 'product',
      component: Produk
    }
  ]
})
router.beforeEach((to, from, next) => {
  next() // DO IT!
})
export default router
